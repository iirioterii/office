<?php
/**
 * Grid of Options
 */

/**
 * @namespace
 */
namespace Application;

use Bluz\Proxy\Layout;

return
/**
 * @privilege Management
 * @return void
 */
function () use ($view) {
    /**
     * @var Bootstrap $this
     * @var \Bluz\View\View $view
     */
    Layout::setTemplate('administration.phtml');
    Layout::breadCrumbs(
        [
            $view->ahref('Administration', ['administration', 'index']),
            __('Options')
        ]
    );
    $grid = new Options\Grid();
    $view->grid = $grid;
};
